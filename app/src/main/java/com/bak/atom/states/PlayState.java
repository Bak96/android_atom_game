package com.bak.atom.states;

import android.graphics.Canvas;
import android.view.MotionEvent;

import com.bak.atom.managers.GameManager;
import com.bak.atom.utils.Vector2d;

public abstract class PlayState extends GameState {
    public PlayState(GameManager gameManager) {
        super(gameManager);
    }

    public abstract void reset();
    public abstract void start();
    public abstract void back();
    public abstract void next();
    public abstract void spawnBullet(int x, int y, int radius, Vector2d velocity);
}
