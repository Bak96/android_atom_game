package com.bak.atom.states;

import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;

import com.bak.atom.AmmoBonus;
import com.bak.atom.Ball;
import com.bak.atom.Bonus;
import com.bak.atom.Box;
import com.bak.atom.Chest;
import com.bak.atom.ChestDestroyedEffect;
import com.bak.atom.Constants;
import com.bak.atom.Effect;
import com.bak.atom.Enemy;
import com.bak.atom.Key;
import com.bak.atom.Player;
import com.bak.atom.ShieldBonus;
import com.bak.atom.UserInterface;
import com.bak.atom.managers.CollisionManager;
import com.bak.atom.managers.GameManager;
import com.bak.atom.utils.Vector2d;

import java.util.ArrayList;
import java.util.Random;

public class EndlessState extends PlayState{
    private UserInterface ui;
    private CollisionManager collider;
    private boolean gameOver;

    private Player player;
    private ArrayList<Enemy> enemies;
    private ArrayList<Box> obstacles;
    private ArrayList<Ball> bullets;
    private ArrayList<Chest> chests; //boxes that can be destroyed
    private ArrayList<Bonus> bonuses;
    private ArrayList<Effect> effects;

    private Chest key_chest;
    private Key key;

    private Random rnd;
    private boolean started;

    private long shieldSpawnedTime;
    private long shieldWaitsToSpawn;

    private long ammoSpawnedTime;
    private long ammoWaitsToSpawn;

    private int bulletsSpawned;
    private int bulletsHit;

    private int score;
    private SharedPreferences sharedPreferences;
    private String level;

    private long keyCollectedTime;

    private long enemySpawnedTime;
    private long enemyWaitsToSpawn;
    private int maxEnemies;

    public EndlessState(GameManager gameManager) {
        super(gameManager);
        sharedPreferences = gameManager.getSharedPreferences();
        level = "Endless";

        reset();
    }

    @Override
    public boolean touched(MotionEvent event) {
        return ui.touched(event);
    }

    public void update() {
        if (gameOver || !started) return;

        player.update();
        for (Enemy e : enemies) {
            e.update();
        }

        for (Bonus bonus : bonuses) {
            bonus.update();
        }

        for (Ball bullet : bullets) {
            bullet.update();
        }

        for (int i = 0; i < effects.size(); i++) {
            effects.get(i).update();
            if (effects.get(i).isFinished()) {
                effects.remove(i);
                System.out.println("effect removed");
                i--;
            }
        }

        checkCollisions();

        if (player.getHealth() == 0) {
            gameOver = true;
            ui.setMode(UserInterface.Mode.GAME_OVER);
            String text = "";
            if (sharedPreferences.getInt("level_" + level, 0) < score) {
                text = "RECORD! ";
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt("level_" + level, score);
                editor.commit();
            }

            if (score == 1) {
                text += "1 key collected";
            }
            else {
                text += Integer.toString(score) + " keys collected";
            }
            ui.printInfo(text, 99999999);
        }

        for (int i = 0; i < chests.size(); i++) {
            if (chests.get(i).getHealth() == 0) {
                Chest c = chests.remove(i);
                effects.add(new ChestDestroyedEffect(c.getPosition(), (int)c.getWidth(), (int)c.getHeight()));
                i--;
            }
        }

        //key_chest destroyed through this update
        if (key_chest != null) {
            if (key_chest.getHealth() == 0) {
                effects.add(new ChestDestroyedEffect(key_chest.getPosition(), (int)key_chest.getWidth(), (int)key_chest.getHeight()));
                key_chest = null;
            }
        }

        //spawn shield bonus
        if (System.currentTimeMillis() > shieldSpawnedTime + shieldWaitsToSpawn) {
            spawnShieldBonus();
        }

        //spawn ammo bonus
        if (System.currentTimeMillis() > ammoSpawnedTime + ammoWaitsToSpawn) {
            spawnAmmoBonus();
        }

        //spawn chest
        if (key == null && System.currentTimeMillis() > keyCollectedTime + 5000) {
            spawnChestKey();
        }

        //spawn enemy
        if (enemies.size() < maxEnemies && System.currentTimeMillis() > enemySpawnedTime + enemyWaitsToSpawn) {
            spawnEnemy();
        }

        ui.update();
    }

    @Override
    public void draw(Canvas canvas) {
        //drawing obstacles
        for (Box box : obstacles) {
            box.draw(canvas);
        }

        //drawing bullets
        for (Ball bullet: bullets) {
            bullet.draw(canvas);
        }

        //drawing key_chest or key if key_chest is destroyed
        if (key_chest != null) {
            key_chest.draw(canvas);
        } else if (key != null){
            key.draw(canvas);
        }

        //drawing chests
        for (Chest chest : chests) {
            chest.draw(canvas);
        }

        //drawing shield bonuses
        for (Bonus bonus : bonuses) {
            bonus.draw(canvas);
        }

        //drawing player
        player.draw(canvas);

        //drawing enemies
        for (Enemy e: enemies) {
            e.draw(canvas);
        }

        //drawing effects
        for (Effect effect : effects) {
            effect.draw(canvas);
        }

        ui.draw(canvas);
    }

    private void checkCollisions() {
        //enemy hit player
        for (Enemy e: enemies) {
            collider.checkBorderHit(e);
            if (collider.colliding(player, e)) {
                if (!player.isUsingShield()) {
                    player.hit(10);
                }
                collider.resolveCollision(player, e);
            }
        }

        //enemy hit enemy
        for (int i = 0; i < enemies.size(); i++)
        {
            for (int j = i + 1; j < enemies.size(); j++)
            {
                if (collider.colliding(enemies.get(i), enemies.get(j))) {
                    collider.resolveCollision(enemies.get(i), enemies.get(j));
                }
            }
        }

        //enemy hit obstacle or chest or key_chest
        for(Enemy enemy: enemies) {
            for(Box box: obstacles) {
                if (collider.colliding(enemy, box)) {
                    collider.resolveCollision(enemy, box);
                }
            }

            for(Box box : chests) {
                if (collider.colliding(enemy, box)) {
                    collider.resolveCollision(enemy, box);
                }
            }

            if (key_chest != null) {
                if (collider.colliding(enemy, key_chest)) {
                    collider.resolveCollision(enemy, key_chest);
                }
            }
        }

        //bonus Collision

        for (int i = 0; i < bonuses.size(); i++) {
            collider.checkBorderHit(bonuses.get(i));
            for (Enemy enemy : enemies) {
                if (collider.colliding(bonuses.get(i), enemy)) {
                    collider.resolveCollision(bonuses.get(i), enemy);
                }
            }

            for (Box box: obstacles) {
                if (collider.colliding(bonuses.get(i), box)) {
                    collider.resolveCollision(bonuses.get(i), box);
                }
            }

            for (Chest chest: chests) {
                if (collider.colliding(bonuses.get(i), chest)) {
                    collider.resolveCollision(bonuses.get(i), chest);
                }
            }

            if (key_chest != null) {
                if (collider.colliding(bonuses.get(i), key_chest)) {
                    collider.resolveCollision(bonuses.get(i), key_chest);
                }
            }

            if (collider.colliding(player, bonuses.get(i))) {
                bonuses.get(i).applyBonus(player);
                bonuses.remove(bonuses.get(i));
                i--;
            }
        }

        //player border collision
        collider.checkBorderHit(player);

        //player hit key_chest or hit key
        if (key_chest != null) {
            if (collider.colliding(player, key_chest)) {
                collider.resolveCollision(player, key_chest);
            }
        }
        else {
            if (key != null && collider.colliding(player, key)) {
                collectKey();
            }
        }

        for (int i = 0; i < chests.size(); i++) {
            if (collider.colliding(player, chests.get(i))) {
                collider.resolveCollision(player, chests.get(i));
            }
        }

        //player hit obstacle
        for (Box box : obstacles) {
            if (collider.colliding(player, box)) {
                collider.resolveCollision(player, box);
            }
        }

        //bullets
        for (int i = 0; i < bullets.size(); i++) {
            if (collider.checkBorderHit(bullets.get(i))) {
                bullets.remove(i);
                i--;
                continue;
            }

            if (key_chest != null) {
                if (collider.colliding(bullets.get(i), key_chest)) {
                    bullets.remove(i);
                    bulletsHit++;
                    i--;
                    key_chest.hit(10);
                    continue;
                }
            }

            boolean bulletHit = false;
            for (int j = 0; j < chests.size(); j++) {
                if (collider.colliding(bullets.get(i), chests.get(j))) {
                    bullets.remove(i);
                    bulletsHit++;
                    i--;
                    chests.get(j).hit(10);

                    bulletHit = true;
                    break;
                }
            }
            if (bulletHit) {
                continue;
            }

            for (Enemy enemy : enemies) {
                if (collider.colliding(bullets.get(i), enemy)) {
                    collider.resolveCollision(bullets.get(i), enemy);
                    bulletsHit++;
                    bullets.remove(i);
                    bulletHit = true;
                    i--;
                    break;
                }
            }

            if (bulletHit) {
                continue;
            }

            for (Box obstacle : obstacles) {
                if (collider.colliding(bullets.get(i), obstacle)) {
                    bullets.remove(i);
                    i--;
                }
            }
        }
    }

    public void start() {
        started = true;
        ui.setMode(UserInterface.Mode.PLAY);
    }

    private void collectKey() {
        key = null;
        score++;
        keyCollectedTime = System.currentTimeMillis();

        if (score == 1) {
            ui.printInfo("1 key collected" , 3000);
        }
        else {
            ui.printInfo(Integer.toString(score) + " keys collected", 3000);
        }


    }

    public void reset() {
        gameOver = false;
        started = false;
        rnd = new Random();
        collider = new CollisionManager();
        enemies = new ArrayList<>();
        obstacles = new ArrayList<>();
        bullets = new ArrayList<>();
        chests = new ArrayList<>();
        bonuses = new ArrayList<>();
        effects = new ArrayList<>();
        enemySpawnedTime = System.currentTimeMillis();
        enemyWaitsToSpawn = 45000;
        maxEnemies = 5;

        score = 0;
        player = new Player(Constants.GAME_WIDTH / 2, Constants.GAME_HEIGHT / 2, 50, 100, 7, 30, this);
        spawnChestKey();

        ui = new UserInterface(this, player, gameManager.getResources(), level);
        ui.setMode(UserInterface.Mode.BEFORE_START);

        shieldSpawnedTime = System.currentTimeMillis();
        ammoSpawnedTime = System.currentTimeMillis();

        shieldWaitsToSpawn = (rnd.nextInt(15) + 5) * 1000;
        ammoWaitsToSpawn = (rnd.nextInt(15) + 5) * 1000;
    }

    public void spawnBullet(int x, int y, int radius, Vector2d velocity) {
        Ball bullet = new Ball(x, y, radius);
        bullet.setVelocity(velocity);
        bullet.setColor(Color.GRAY);
        bullet.setMass(0.2f);
        bullets.add(bullet);
        bulletsSpawned++;
    }

    public void back() {
        gameManager.clearStates();
        gameManager.pushState(GameManager.State.LEVEL_SELECT);
    }

    private void spawnShieldBonus() {
        boolean colliding = true;
        Bonus b = new ShieldBonus(0, 0, 25);

        while (colliding) {
            colliding = false;
            b.setPosition(new Vector2d(rnd.nextInt(Constants.GAME_WIDTH - 60) + 30, rnd.nextInt(Constants.GAME_HEIGHT - 60) + 60));

            if (collider.colliding(b, player)) {
                colliding = true;
            }

            for (Box obstacle : obstacles) {
                if (collider.colliding(b, obstacle)) {
                    colliding = true;
                    break;
                }
            }

            for (Chest chest : chests) {
                if (collider.colliding(b, chest) || colliding) {
                    colliding = true;
                    break;
                }
            }
        }

        bonuses.add(b);
        shieldSpawnedTime = System.currentTimeMillis();
        shieldWaitsToSpawn = (rnd.nextInt(30) + 12) * 1000;
    }

    private void spawnAmmoBonus() {
        boolean colliding = true;
        Bonus b = new AmmoBonus(0, 0, 25);

        while (colliding) {
            colliding = false;
            b.setPosition(new Vector2d(rnd.nextInt(Constants.GAME_WIDTH - 60) + 30, rnd.nextInt(Constants.GAME_HEIGHT - 60) + 60));

            if (collider.colliding(b, player)) {
                colliding = true;
            }

            for (Box obstacle : obstacles) {
                if (collider.colliding(b, obstacle)) {
                    colliding = true;
                    break;
                }
            }

            for (Chest chest : chests) {
                if (collider.colliding(b, chest) || colliding) {
                    colliding = true;
                    break;
                }
            }
        }

        ammoSpawnedTime = System.currentTimeMillis();
        ammoWaitsToSpawn = (rnd.nextInt(30) + 10) * 1000;

        bonuses.add(b);
    }

    private void spawnChestKey() {
        int chestWidth = rnd.nextInt(200) + 10;
        int chestHeight = rnd.nextInt(200) + 10;
        int chestX = rnd.nextInt(Constants.GAME_WIDTH - chestWidth);
        int chestY = rnd.nextInt(Constants.GAME_HEIGHT - chestHeight);
        int chestHealth = rnd.nextInt(200) + 10;

        key_chest = new Chest(chestX, chestY, chestWidth, chestHeight, chestHealth);
        key = new Key((int)key_chest.getPosition().getX(), (int)key_chest.getPosition().getY(), gameManager.getResources());
        int keyX = (int)(key_chest.getPosition().getX() +((key_chest.getWidth() - key.getWidth()) / 2));
        int keyY = (int)(key_chest.getPosition().getY() + ((key_chest.getHeight() - key.getHeight()) / 2));
        key.setPosition(new Vector2d(keyX, keyY));
    }

    private void spawnEnemy() {
        boolean colliding = true;
        int enemyRadius = rnd.nextInt(25) + 15;

        Enemy e = new Enemy(rnd.nextInt(Constants.GAME_WIDTH - 2*enemyRadius) + enemyRadius, rnd.nextInt(Constants.GAME_HEIGHT - 2*enemyRadius) + enemyRadius, enemyRadius);

        while (colliding) {
            colliding = false;
            e.setPosition(new Vector2d(rnd.nextInt(Constants.GAME_WIDTH - 2*enemyRadius) + enemyRadius, rnd.nextInt(Constants.GAME_HEIGHT - 2*enemyRadius) + enemyRadius));

            if (collider.colliding(e, player)) {
                colliding = true;
            }
        }

        enemies.add(e);
        enemySpawnedTime = System.currentTimeMillis();
    }

    public void next() {

    }
}
