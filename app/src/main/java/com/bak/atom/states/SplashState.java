package com.bak.atom.states;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.MotionEvent;

import com.bak.atom.Constants;
import com.bak.atom.R;
import com.bak.atom.managers.GameManager;

public class SplashState extends GameState {
    private Bitmap logo;
    private long startTime;


    public SplashState(GameManager gameManager) {
        super(gameManager);
        logo = BitmapFactory.decodeResource(gameManager.getResources(), R.drawable.big_logo);
        startTime = System.currentTimeMillis();
    }

    @Override
    public void update() {
        if (System.currentTimeMillis() - startTime > 3000) {
            gameManager.clearStates();
            gameManager.pushState(GameManager.State.MENU);
        }
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawBitmap(logo, Constants.GAME_WIDTH / 2 - logo.getWidth() / 2, (Constants.GAME_HEIGHT + Constants.PANEL_HEIGHT) / 2 - logo.getHeight() / 2, null);
    }

    @Override
    public boolean touched(MotionEvent event) {
        return false;
    }
}
