package com.bak.atom.states;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.TextPaint;
import android.view.MotionEvent;

import com.bak.atom.Constants;
import com.bak.atom.R;
import com.bak.atom.managers.GameManager;
import com.bak.atom.utils.Utils;

/**
 * Created by Home on 2017-08-04.
 */

public class MenuState extends GameState{
    private Rect textBounds;
    private Paint textPaint;

    private Paint buttonPaint;
    private Paint selectionPaint;

    private Rect buttonBounds;

    private Rect playBounds;
    private Rect endlessBounds;
    private Rect leaderboardsBounds;
    private Rect exitBounds;

    private Bitmap logo;


    //reference
    private Rect selection;


    public MenuState(GameManager gameManager) {
        super(gameManager);
        textPaint = new TextPaint();
        textBounds = new Rect();
        buttonBounds = new Rect();
        Utils.setRectWidth(600, buttonBounds);
        Utils.setRectHeight(90, buttonBounds);

        buttonPaint = new Paint();
        buttonPaint.setColor(Color.rgb(51, 128, 153));
        buttonPaint.setStyle(Paint.Style.STROKE);

        selectionPaint = new Paint();
        selectionPaint.setColor(Color.WHITE);
        selectionPaint.setStyle(Paint.Style.STROKE);


        logo = BitmapFactory.decodeResource(gameManager.getResources(), R.drawable.logo);

        createButtons();
    }

    @Override
    public void update() {
    }

    @Override
    public void draw(Canvas canvas) {
        String text;
        textPaint.setTextAlign(Paint.Align.LEFT);
        textPaint.setTextSize(64);
        textPaint.setColor(Color.rgb(51, 128, 153));
        //textPaint.setStyle(Paint.Style.STROKE);

        canvas.drawRect(playBounds, buttonPaint);
        canvas.drawRect(endlessBounds, buttonPaint);
        canvas.drawRect(leaderboardsBounds, buttonPaint);
        canvas.drawRect(exitBounds, buttonPaint);

        text = "Play";
        textPaint.getTextBounds(text, 0, text.length(), textBounds);
        canvas.drawText(text, Constants.GAME_WIDTH / 2 - textBounds.width() / 2, playBounds.bottom - (playBounds.height() - textBounds.height()), textPaint);

        text = "Endless mode";
        textPaint.getTextBounds(text, 0, text.length(), textBounds);
        canvas.drawText(text, Constants.GAME_WIDTH / 2 - textBounds.width() / 2, endlessBounds.bottom - (endlessBounds.height() - textBounds.height()) / 2, textPaint);

        text = "Leaderboards";
        textPaint.getTextBounds(text, 0, text.length(), textBounds);
        canvas.drawText(text, Constants.GAME_WIDTH / 2 - textBounds.width() / 2, leaderboardsBounds.bottom - (leaderboardsBounds.height() - textBounds.height()) / 2, textPaint);

        text = "Exit";
        textPaint.getTextBounds(text, 0, text.length(), textBounds);
        canvas.drawText(text, Constants.GAME_WIDTH / 2 - textBounds.width() / 2, exitBounds.bottom - (exitBounds.height() - textBounds.height()) / 2, textPaint);

        canvas.drawBitmap(logo, Constants.GAME_WIDTH / 2 - logo.getWidth() / 2 , 100, null);

        if (selection != null) {
            canvas.drawRect(selection, selectionPaint);
        }

    }

    @Override
    public boolean touched(MotionEvent event) {
        int scaledX = (int) (event.getX() / Constants.SCALE_X);
        int scaledY = (int) (event.getY() / Constants.SCALE_Y);

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (playBounds.contains(scaledX, scaledY)) {
                    selection = playBounds;
                }
                else if (endlessBounds.contains(scaledX, scaledY)) {
                    selection = endlessBounds;
                }
                else if (leaderboardsBounds.contains(scaledX, scaledY)) {
                    selection = leaderboardsBounds;
                }
                else if (exitBounds.contains(scaledX, scaledY)) {
                    selection = exitBounds;
                }
                return true;
            case MotionEvent.ACTION_UP:
                if (playBounds.contains(scaledX, scaledY)) {
                    gameManager.clearStates();
                    gameManager.pushState(GameManager.State.LEVEL_SELECT);
                }
                else if (endlessBounds.contains(scaledX, scaledY)) {
                    gameManager.clearStates();
                    gameManager.pushState(GameManager.State.ENDLESS);
                }
                else if (leaderboardsBounds.contains(scaledX, scaledY)) {
                    gameManager.clearStates();
                    gameManager.pushState(GameManager.State.LEADERBOARDS);
                }
                else if (exitBounds.contains(scaledX, scaledY)) {
                    gameManager.exit();
                }

                selection = null;
                return true;
            case MotionEvent.ACTION_MOVE:
                selection = null;
                if (playBounds.contains(scaledX, scaledY)) {
                    selection = playBounds;
                }
                else if (endlessBounds.contains(scaledX, scaledY)) {
                    selection = endlessBounds;
                }
                else if (leaderboardsBounds.contains(scaledX, scaledY)) {
                    selection = leaderboardsBounds;
                }
                else if (exitBounds.contains(scaledX, scaledY)) {
                    selection = exitBounds;
                }
                return true;
        }

        return false;
    }

    private void createButtons() {
        playBounds = new Rect();
        Utils.setRectWidth(600, playBounds);
        Utils.setRectHeight(90, playBounds);
        Utils.setRectX(Constants.GAME_WIDTH / 2 - playBounds.width() / 2, playBounds);
        Utils.setRectY((Constants.GAME_HEIGHT + Constants.PANEL_HEIGHT) / 3 - 1, playBounds);

        endlessBounds = new Rect();
        Utils.setRectWidth(600, endlessBounds);
        Utils.setRectHeight(90, endlessBounds);
        Utils.setRectX(Constants.GAME_WIDTH / 2 - playBounds.width() / 2, endlessBounds);
        Utils.setRectY((Constants.GAME_HEIGHT+ Constants.PANEL_HEIGHT) / 3 + 150, endlessBounds);

        leaderboardsBounds = new Rect();
        Utils.setRectWidth(600, leaderboardsBounds);
        Utils.setRectHeight(90, leaderboardsBounds);
        Utils.setRectX(Constants.GAME_WIDTH / 2 - playBounds.width() / 2, leaderboardsBounds);
        Utils.setRectY((Constants.GAME_HEIGHT + Constants.PANEL_HEIGHT)/ 3 + 300, leaderboardsBounds);

        exitBounds = new Rect();
        Utils.setRectWidth(600, exitBounds);
        Utils.setRectHeight(90, exitBounds);
        Utils.setRectX(Constants.GAME_WIDTH / 2 - playBounds.width() / 2, exitBounds);
        Utils.setRectY((Constants.GAME_HEIGHT + Constants.PANEL_HEIGHT)/ 3 + 450, exitBounds);
    }
}
