package com.bak.atom.states;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.constraint.solver.widgets.Rectangle;
import android.view.MotionEvent;

import com.bak.atom.Constants;
import com.bak.atom.R;
import com.bak.atom.managers.GameManager;
import com.bak.atom.utils.Utils;

/**
 * Created by Home on 2017-08-07.
 */

public class LeaderboardsState extends GameState {
    private Rect rectangle;
    private Rect textBounds;
    private Rect backButton;
    private Rect background;
    private String backButtonText;

    private Paint rectanglePaint;
    private Paint numberPaint;
    private Paint scorePaint;
    private Paint backgroundPaint;
    private Paint selectedPaint;

    private int rectangleSize = 100;
    private String text;

    private float offset;
    private float targetOffset;

    private float lastY;

    private Bitmap okBitmap;
    private Bitmap wrongBitmap;
    private SharedPreferences sharedPreferences;
    private boolean backButtonSelected = false;

    private int score;

    public LeaderboardsState(GameManager gameManager) {
        super(gameManager);

        selectedPaint = new Paint();
        selectedPaint.setColor(Color.WHITE);
        selectedPaint.setStyle(Paint.Style.STROKE);

        backgroundPaint = new Paint();
        backgroundPaint.setColor(Color.BLACK);
        backgroundPaint.setStyle(Paint.Style.FILL);

        rectanglePaint = new Paint();
        rectanglePaint.setColor(Color.rgb(51, 128, 153));
        rectanglePaint.setStyle(Paint.Style.STROKE);
        rectangle = new Rect();
        Utils.setRectWidth(100, rectangle);
        Utils.setRectHeight(100, rectangle);
        Utils.setRectX(20, rectangle);

        numberPaint = new Paint();
        numberPaint.setColor(Color.rgb(51, 128, 153));
        numberPaint.setTextSize(64);

        scorePaint = new Paint();
        scorePaint.setColor(Color.WHITE);
        scorePaint.setTextSize(64);

        textBounds = new Rect();

        offset = 0;
        targetOffset = 0;

        okBitmap = BitmapFactory.decodeResource(gameManager.getResources(), R.drawable.ok);
        wrongBitmap = BitmapFactory.decodeResource(gameManager.getResources(), R.drawable.wrong);

        sharedPreferences = gameManager.getSharedPreferences();

        backButton = new Rect();
        Utils.setRectWidth(300, backButton);
        Utils.setRectHeight(90, backButton);
        Utils.setRectX(Constants.GAME_WIDTH - 20 - backButton.width(), backButton);
        Utils.setRectY(Constants.GAME_HEIGHT + Constants.PANEL_HEIGHT - 100, backButton);

        background = new Rect();
        Utils.setRectWidth(backButton.width(), background);
        Utils.setRectHeight(backButton.height(), background);
        Utils.setRectX(backButton.left, background);
        Utils.setRectY(backButton.top, background);
        backButtonText = "Back";
    }

    @Override
    public void update() {
        if (targetOffset > 0) {
            targetOffset = 0;
        }
        if (targetOffset < -16*130) {
            targetOffset = -16*130;
        }

        offset = offset + (targetOffset - offset) * .2f;
    }

    @Override
    public void draw(Canvas canvas) {

        for (int i = 0; i < 26; i++) {
            canvas.drawLine(20, 15 + i * 120 + (int)offset, Constants.GAME_WIDTH - 20, 15 + i * 120 + (int)offset, rectanglePaint);
            Utils.setRectY(25 + i*120 + (int)offset, rectangle);
            canvas.drawRect(rectangle, rectanglePaint);

            if (i <= 9) {
                text = "0" + i;
            }
            else {
                text = Integer.toString(i);
            }

            if (i == 0) {
                text = "E";
            }


            numberPaint.getTextBounds(text, 0, text.length(), textBounds);
            canvas.drawText(text, rectangle.centerX() - textBounds.width() / 2, rectangle.centerY() + textBounds.height()/2, numberPaint);

            if (i == 0) {
                score = sharedPreferences.getInt("level_Endless", 0);
            }
            else {
                score = sharedPreferences.getInt("level_"+ text, 0);
            }

            if (score == 0) {
                canvas.drawBitmap(wrongBitmap, 150, 20 + i*120 + (int) offset, null);
            }
            else {
                text = Integer.toString(score);
                canvas.drawBitmap(okBitmap, 150, 20 + i*120 + (int) offset, null);
                scorePaint.getTextBounds(text, 0, text.length(), textBounds);
                canvas.drawText(text, 300, rectangle.centerY() + textBounds.height() / 2, scorePaint);
            }

            canvas.drawRect(background, backgroundPaint);

            numberPaint.getTextBounds(backButtonText, 0, backButtonText.length(), textBounds);
            canvas.drawText(backButtonText, backButton.centerX() - textBounds.width() / 2, backButton.centerY() + textBounds.height() / 2, numberPaint);
            if (backButtonSelected) {
                canvas.drawRect(backButton, selectedPaint);
            }
            else {
                canvas.drawRect(backButton, rectanglePaint);
            }
        }
    }

    @Override
    public boolean touched(MotionEvent event) {
        int x = (int)(event.getX() / Constants.SCALE_X);
        int y = (int)(event.getY() / Constants.SCALE_Y);

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (backButton.contains(x, y)) {
                    backButtonSelected = true;
                } else {
                    lastY = y;
                }

                return true;
            case MotionEvent.ACTION_UP:
                if (backButton.contains(x, y)) {
                    gameManager.clearStates();
                    gameManager.pushState(GameManager.State.MENU);
                }

                return true;
            case MotionEvent.ACTION_MOVE:
                if (backButtonSelected) {
                    if (!backButton.contains(x, y)) {
                        backButtonSelected = false;
                    }
                }
                else {
                    targetOffset += y - lastY;
                    lastY = y;
                }

                return true;
        }

        return false;
    }
}
