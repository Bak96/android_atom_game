package com.bak.atom.states;

import android.graphics.Canvas;
import android.view.MotionEvent;

import com.bak.atom.managers.GameManager;

public abstract class GameState {

    //reference
    protected GameManager gameManager;
    public GameState(GameManager gameManager) {
        this.gameManager = gameManager;
    }
    public abstract void update();
    public abstract void draw(Canvas canvas);
    public abstract boolean touched(MotionEvent event);
}
