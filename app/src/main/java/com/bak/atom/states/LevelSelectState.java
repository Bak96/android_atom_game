package com.bak.atom.states;

import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;

import com.bak.atom.Constants;
import com.bak.atom.managers.GameManager;
import com.bak.atom.utils.Utils;

import java.util.ArrayList;

public class LevelSelectState extends GameState{
    private ArrayList<Rect> levelBounds;

    private Paint levelBoundPaint;
    private Paint numberPaint;
    private Paint titlePaint;
    private Paint selectionPaint;
    private Paint winPaint;
    private Paint winTextPaint;
    private String title;

    private final int buttonSize = 100;
    private final int padding = 20;

    private Rect textBounds;
    private Rect backButton;
    private boolean backButtonSelected;
    private String backButtonText;
    private SharedPreferences sharedPreferences;

    private int selection;

    public LevelSelectState(GameManager gameManager) {
        super(gameManager);
        levelBounds = new ArrayList<>();
        levelBoundPaint = new Paint();
        levelBoundPaint.setColor(Color.rgb(51, 128, 153));
        levelBoundPaint.setStyle(Paint.Style.STROKE);

        selectionPaint = new Paint();
        selectionPaint.setColor(Color.WHITE);
        selectionPaint.setStyle(Paint.Style.STROKE);

        numberPaint = new Paint();
        numberPaint.setColor(Color.rgb(51, 128, 153));
        numberPaint.setTextSize(64);

        winPaint = new Paint();
        winPaint.setColor(Color.rgb(0, 255, 153));
        winPaint.setStyle(Paint.Style.STROKE);

        winTextPaint = new Paint();
        winTextPaint.setColor(Color.rgb(0, 255, 153));
        winTextPaint.setTextSize(64);

        titlePaint = new Paint();
        titlePaint.setColor(Color.WHITE);
        titlePaint.setTextSize(86);

        int beginXPos = (Constants.GAME_WIDTH - 5 * buttonSize - 4 * padding) / 2;
        int beginYPos = ((Constants.GAME_HEIGHT + Constants.PANEL_HEIGHT) - 5 * buttonSize - 4 * padding) * 2 / 3;
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                Rect bounds = new Rect();
                Utils.setRectX(beginXPos + (buttonSize + padding) * j, bounds);
                Utils.setRectY(beginYPos + i*(buttonSize + padding), bounds);
                Utils.setRectWidth(buttonSize, bounds);
                Utils.setRectHeight(buttonSize, bounds);
                levelBounds.add(bounds);
            }
        }

        backButton = new Rect();
        Utils.setRectWidth(300, backButton);
        Utils.setRectHeight(90, backButton);
        Utils.setRectX(levelBounds.get(0).left, backButton);
        Utils.setRectY(Constants.GAME_HEIGHT + Constants.PANEL_HEIGHT - 50 - 100, backButton);

        textBounds = new Rect();

        selection = -1;
        title = "Select level";
        backButtonText = "Back";
        backButtonSelected = false;

        sharedPreferences = gameManager.getSharedPreferences();
    }

    @Override
    public void update() {

    }

    @Override
    public void draw(Canvas canvas) {
        titlePaint.getTextBounds(title, 0, title.length(), textBounds);
        canvas.drawText(title, (Constants.GAME_WIDTH - textBounds.width()) / 2, (Constants.GAME_HEIGHT + Constants.PANEL_HEIGHT) / 5, titlePaint);

        for (int i = 0; i < levelBounds.size(); i++) {
            String number;

            if (i < 9) {
                number = "0" + Integer.toString(i+1);
            }
            else {
                number = Integer.toString(i+1);
            }

            if (i == selection) {
                canvas.drawRect(levelBounds.get(i), selectionPaint);
            }//level completed
            else if (sharedPreferences.getInt("level_" + number, 0) > 0){
                canvas.drawRect(levelBounds.get(i), winPaint);
            }
            else {
                canvas.drawRect(levelBounds.get(i), levelBoundPaint);
            }


            if (sharedPreferences.getInt("level_" + number, 0) > 0) {
                winTextPaint.getTextBounds(number, 0, number.length(), textBounds);
                canvas.drawText(number, levelBounds.get(i).centerX() - textBounds.width() / 2, levelBounds.get(i).centerY() + textBounds.height() / 2, winTextPaint);
            }
            else {
                numberPaint.getTextBounds(number, 0, number.length(), textBounds);
                canvas.drawText(number, levelBounds.get(i).centerX() - textBounds.width() / 2, levelBounds.get(i).centerY() + textBounds.height() / 2, numberPaint);
            }

        }

        numberPaint.getTextBounds(backButtonText, 0, backButtonText.length(), textBounds);
        canvas.drawText(backButtonText, backButton.centerX() - textBounds.width() / 2, backButton.centerY() + textBounds.height() / 2, numberPaint);

        if (backButtonSelected) {
            canvas.drawRect(backButton, selectionPaint);
        }
        else {
            canvas.drawRect(backButton, levelBoundPaint);
        }
    }

    @Override
    public boolean touched(MotionEvent event) {
        int x = (int)(event.getX() / Constants.SCALE_X);
        int y = (int)(event.getY() / Constants.SCALE_Y);

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                for (int i = 0; i < levelBounds.size(); i++) {
                    if (levelBounds.get(i).contains(x, y)) {
                        selection = i;
                        break;
                    }
                }

                if (backButton.contains(x, y)) {
                    backButtonSelected = true;
                }

                return true;
            case MotionEvent.ACTION_UP:
                for (int i = 0; i < levelBounds.size(); i++) {
                    if (levelBounds.get(i).contains(x, y)) {
                        selection = i;
                        System.out.println(i+1 + " pressed");
                        gameManager.clearStates();
                        String levelName = Integer.toString(i+1);
                        if (levelName.length() == 1) {
                            levelName = "0" + levelName;
                        }
                        gameManager.playState(levelName);
                        return true;
                    }

                }

                if (backButton.contains(x, y)) {
                    gameManager.clearStates();
                    gameManager.pushState(GameManager.State.MENU);
                }

                backButtonSelected = false;
                selection = -1;
                return true;
            case MotionEvent.ACTION_MOVE:
                selection = -1;
                backButtonSelected = false;
                for (int i = 0; i < levelBounds.size(); i++) {
                    if (levelBounds.get(i).contains(x, y)) {
                        selection = i;
                        break;
                    }
                }

                if (backButton.contains(x, y)) {
                    backButtonSelected = true;
                }

                return true;
        }
        return false;
    }
}
