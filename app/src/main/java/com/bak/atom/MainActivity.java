package com.bak.atom;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends Activity {
    View mDecorView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        Constants.SCREEN_WIDTH = dm.widthPixels;
        Constants.SCREEN_HEIGHT = dm.heightPixels;

        float factor = getResources().getDisplayMetrics().density;
        Constants.SCREEN_DENSITY = factor;
        System.out.println("widthPX: " + dm.widthPixels + " heightPX: " + dm.heightPixels + " density: " + factor);


        Constants.GAME_WIDTH = 720;
        Constants.GAME_HEIGHT = 1184 - 200;
        Constants.PANEL_WIDTH = 720;
        Constants.PANEL_HEIGHT = 200;

        Constants.SCALE_X = (float)Constants.SCREEN_WIDTH / (float)Constants.GAME_WIDTH;
        Constants.SCALE_Y = (float)Constants.SCREEN_HEIGHT / (float)(Constants.GAME_HEIGHT + Constants.PANEL_HEIGHT);

        mDecorView = getWindow().getDecorView();

        setContentView(new GamePanel(this, getAssets(), getPreferences(Context.MODE_PRIVATE)));
    }


/*
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (hasFocus) {
            mDecorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }*/
}
