package com.bak.atom;

import android.graphics.Canvas;
import android.graphics.Color;

import com.bak.atom.utils.Vector2d;

public abstract class GameObject {

    protected Vector2d position;
    protected Vector2d velocity;
    protected int color;
    protected float mass;

    public GameObject(int x, int y) {
        position = new Vector2d(x, y);
        float direction = (float)(Math.random() * 2 * Math.PI);

        float dx = (float) Math.cos(direction);
        float dy = (float) Math.sin(direction);
        velocity = new Vector2d(dx * 10, dy*10);

        setColor(Color.YELLOW);
        this.mass = 1;
    }

    public void setDirection(int directionX, int directionY) {
        float dx = directionX - position.getX();
        float dy = directionY - position.getY();

        setDirection(Math.atan2(dy, dx));
    }

    //radians
    public void setDirection(double direction) {
        Vector2d rotated = new Vector2d((float)Math.cos(direction), (float)Math.sin(direction));
        velocity = rotated.multiply(velocity.getLength());
    }

    public Vector2d getPosition() {
        return position;
    }

    public Vector2d getVelocity() {
        return velocity;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getColor() {
        return color;
    }

    public float getMass() {
        return mass;
    }
    public void setMass(float mass) {
        this.mass = mass;
    }

    public void setPosition(Vector2d position) {
        this.position = position;
    }

    public void setVelocity(Vector2d velocity) {
        this.velocity = velocity;
    }

    public void setSpeed(float speed) {
        if (this.velocity.getX() == 0 && this.velocity.getY() == 0) {
            this.velocity = new Vector2d(1, 0);
        }
        this.velocity = velocity.normalize().multiply(speed);
    }

    public abstract void update();

    public abstract void draw(Canvas canvas);

}
