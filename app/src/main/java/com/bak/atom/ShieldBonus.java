package com.bak.atom;

import android.graphics.Color;

public class ShieldBonus extends Bonus {
    public ShieldBonus(int x, int y, int radius) {
        super(x, y, radius);
        setColor(Color.BLUE);
    }

    @Override
    public void applyBonus(Player player) {
        player.addShield(1);
    }
}
