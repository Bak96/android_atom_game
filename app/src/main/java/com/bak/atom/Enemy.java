package com.bak.atom;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.bak.atom.utils.Vector2d;

/**
 * Created by Home on 2017-07-28.
 */

public class Enemy extends Ball{

    public Enemy(int x, int y, int radius) {
        super(x, y, radius);
        setColor(Color.RED);
    }

    @Override
    public void update() {
        super.update();
    }
}
