package com.bak.atom;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

public class Key extends Box {
    private Bitmap keyBitmap;

    public Key(int x, int y, Resources resources) {
        super(x, y, 0, 0);
        keyBitmap = BitmapFactory.decodeResource(resources, R.drawable.key);
        setWidth(keyBitmap.getWidth());
        setHeight(keyBitmap.getHeight());
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawBitmap(keyBitmap,getPosition().getX(), getPosition().getY(), null);
    }
}
