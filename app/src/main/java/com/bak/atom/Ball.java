package com.bak.atom;

import android.graphics.Canvas;
import android.graphics.Paint;

import com.bak.atom.GameObject;
public class Ball extends GameObject {
    float radius;

    public Ball(int x, int y, int radius) {
        super(x, y);
        this.radius = radius;
    }

    @Override
    public void update() {
        position = position.add(velocity);
    }

    @Override
    public void draw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(color);
        canvas.drawCircle(position.getX(), position.getY(), radius, paint);

    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }
}
