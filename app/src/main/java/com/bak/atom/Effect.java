package com.bak.atom;

import android.graphics.Canvas;

/**
 * Created by Home on 2017-08-04.
 */

public abstract class Effect {
    public abstract void update();
    public abstract void draw(Canvas canvas);
    public abstract boolean isFinished();
}
