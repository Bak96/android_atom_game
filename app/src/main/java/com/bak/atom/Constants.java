package com.bak.atom;

/**
 * Created by Home on 2017-07-27.
 */

public class Constants {
    public static int SCREEN_WIDTH;
    public static int SCREEN_HEIGHT;
    public static float SCREEN_DENSITY;

    public static float SCALE_X;
    public static float SCALE_Y;

    public static int GAME_WIDTH;
    public static int GAME_HEIGHT;

    public static int PANEL_WIDTH;
    public static int PANEL_HEIGHT;

    public static final float restitution = 1;
}
