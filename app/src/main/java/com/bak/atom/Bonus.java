package com.bak.atom;

/**
 * Created by Home on 2017-08-09.
 */

public abstract class Bonus extends Ball {
    public Bonus(int x, int y, int radius) {
        super(x, y, radius);
    }

    public abstract void applyBonus(Player player);
}
