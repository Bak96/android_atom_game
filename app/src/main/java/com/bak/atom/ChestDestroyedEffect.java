package com.bak.atom;

import android.graphics.Canvas;
import android.graphics.Color;

import com.bak.atom.utils.Vector2d;

import java.util.ArrayList;

/**
 * Created by Home on 2017-08-04.
 */

public class ChestDestroyedEffect extends Effect{

    ArrayList<Box> particles = new ArrayList<>();

    public ChestDestroyedEffect(Vector2d position, int width, int height) {
        int offset = 10;
        int particleWidth = width / 4 + offset;
        int particleHeight = height / 4 + offset;

        System.out.println("position: " + position.getX() + " " + position.getY() + " width " + width + " height " + height);
        System.out.println("particleWidth " + particleWidth + " particleHeight " + particleHeight);

        Box particle;

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                particle = new Box((int) position.getX() + j*(particleWidth - offset), (int) position.getY() + i *(particleHeight - offset), particleWidth, particleHeight);
                particle.setSpeed(10);
                particle.setDirection(Math.random() * 2 * Math.PI);
                particles.add(particle);
            }

        }

    }
    @Override
    public void update() {
        for (Box particle : particles) {
            particle.update();
        }
    }

    @Override
    public void draw(Canvas canvas) {
        for (Box particle : particles) {
            particle.draw(canvas);
        }
    }

    public boolean isFinished() {
        for (Box particle : particles) {
            //left
            if (particle.getPosition().getX() + particle.getWidth() > 0 &&
                    particle.getPosition().getX() < Constants.GAME_WIDTH &&
                    particle.getPosition().getY() + particle.getHeight() > 0 &&
                    particle.getPosition().getY() < Constants.GAME_HEIGHT) {
                return false;
            }
        }

        return true;
    }
}
