package com.bak.atom;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;

import com.bak.atom.managers.GameManager;
import com.bak.atom.states.PlayState;
import com.bak.atom.utils.Button;
import com.bak.atom.utils.Utils;

public class UserInterface {
    //references
    private PlayState playState;
    private Player player;

    //paint
    private Paint paint = new Paint();

    //ui components
    private Button restartButton;
    private Button nextLevelButton;
    private Button backButton;

    private Rect textBounds;

    private Rect healthBar = new Rect();

    private Rect actionPanelArea;
    private Rect fireButtonArea;
    private Rect shieldButtonArea;

    private Bitmap actionPanelBitmap;
    private Bitmap fireButtonBitmap;
    private Bitmap shieldButtonBitmap;
    private Bitmap fireButtonPressedBitmap;
    private Bitmap shieldButtonPressedBitmap;
    private Bitmap shieldBitmap;

    //handle
    private Bitmap activeFireBitmap;
    private Bitmap activeShieldBitmap;

    public enum Mode {
        BEFORE_START,
        PLAY,
        WIN,
        GAME_OVER
    }

    private Mode mode;
    private String text;
    private int score;
    private String levelNumber;
    private String info;
    private boolean printingInfo;
    private long printingInfoStart;
    private long printingInfoDuration;

    public UserInterface(PlayState playState, Player player, Resources res, String levelNumber) {
        this.playState = playState;
        this.player = player;
        //draw panel with buttons
        actionPanelArea = new Rect(0, Constants.GAME_HEIGHT, Constants.GAME_WIDTH, Constants.GAME_HEIGHT + Constants.PANEL_HEIGHT);
        actionPanelBitmap = BitmapFactory.decodeResource(res, R.drawable.panel_texture);
        fireButtonBitmap = BitmapFactory.decodeResource(res, R.drawable.fire_button);
        shieldButtonBitmap = BitmapFactory.decodeResource(res, R.drawable.shield_button);
        fireButtonPressedBitmap = BitmapFactory.decodeResource(res, R.drawable.fire_button_pressed);
        shieldButtonPressedBitmap = BitmapFactory.decodeResource(res, R.drawable.shield_button_pressed);
        shieldBitmap = BitmapFactory.decodeResource(res, R.drawable.shield);

        fireButtonArea = new Rect(75, Constants.GAME_HEIGHT + 25, 75 + fireButtonBitmap.getWidth(), Constants.GAME_HEIGHT + 25 + fireButtonBitmap.getHeight());
        shieldButtonArea = new Rect(Constants.PANEL_WIDTH - 75 - shieldButtonBitmap.getWidth(), Constants.GAME_HEIGHT + 25,
                Constants.PANEL_WIDTH - 75, Constants.GAME_HEIGHT + 25 + shieldButtonBitmap.getHeight());

        activeFireBitmap = fireButtonBitmap;
        activeShieldBitmap = shieldButtonBitmap;

        backButton = new Button(10, 10);
        backButton.setWidth(200);
        backButton.setHeight(60);
        backButton.setText("Back");

        int buttonX = (Constants.GAME_WIDTH - 200 - 20 - 200) / 2;
        restartButton = new Button(buttonX, 500);
        restartButton.setWidth(200);
        restartButton.setHeight(60);
        restartButton.setText("Restart");
        restartButton.setTransparent(false);
        nextLevelButton = new Button(buttonX + 200 + 20, 500);
        nextLevelButton.setWidth(200);
        nextLevelButton.setHeight(60);
        nextLevelButton.setText("Next");
        nextLevelButton.setTransparent(false);

        textBounds = new Rect();
        this.levelNumber = levelNumber;
        this.info = "";
        this.printingInfo = false;
    }

    public boolean touched(MotionEvent event) {
        int x = (int)(event.getX() / Constants.SCALE_X);
        int y = (int)(event.getY() / Constants.SCALE_Y);

        switch(event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (backButton.contains(x, y)){
                    backButton.down();
                    return true;
                }

                if (mode == Mode.WIN) {
                    if (restartButton.contains(x, y)) {
                        restartButton.down();
                    }
                    else if (nextLevelButton.contains(x, y)) {
                        nextLevelButton.down();
                    }
                }
                else if (mode == Mode.GAME_OVER) {
                    if (restartButton.contains(x, y)) {
                        restartButton.down();
                    }

                } else if (mode == Mode.BEFORE_START) {
                    playState.start();
                }
                else if (mode == Mode.PLAY){
                    if (actionPanelArea.contains(x, y)) {
                        if (fireButtonArea.contains(x, y)) {
                            player.fire();
                        }
                        else if (shieldButtonArea.contains(x, y)) {
                            player.useShield();
                        }
                    }
                    else {
                        player.setDirection(x, y);
                    }

                }
                return true;
            case MotionEvent.ACTION_MOVE:
                if (backButton.contains(x, y))  {
                    backButton.down();
                }
                else {
                    backButton.up();
                }

                if (mode == Mode.GAME_OVER) {
                    if (restartButton.contains(x, y)) {
                        restartButton.down();
                    }
                    else {
                        restartButton.up();
                    }
                }

                if (mode == Mode.WIN) {
                    if (restartButton.contains(x,y)) {
                        restartButton.down();
                    }
                    else {
                        restartButton.up();
                    }

                    if (nextLevelButton.contains(x, y)) {
                        nextLevelButton.down();
                    }
                    else {
                        nextLevelButton.up();
                    }
                }
                return true;
            case MotionEvent.ACTION_UP:
                if (backButton.contains(x, y)){
                    playState.back();
                }

                if (mode == Mode.GAME_OVER) {
                    if (restartButton.contains(x,y)) {
                        restartButton.up();
                        playState.reset();
                    }


                }

                if (mode == Mode.WIN) {
                    if (restartButton.contains(x, y)) {
                        restartButton.up();
                        playState.reset();
                    }
                    else if (nextLevelButton.contains(x, y)) {
                        nextLevelButton.up();
                        playState.next();
                    }
                }
                return true;
        }

        return false;
    }

    public void update() {
        //buttons update
        if (player.isFiring()) {
            activeFireBitmap = fireButtonPressedBitmap;
        }
        else {
            activeFireBitmap = fireButtonBitmap;
        }

        if (player.isUsingShield()) {
            activeShieldBitmap = shieldButtonPressedBitmap;
        }
        else {
            activeShieldBitmap = shieldButtonBitmap;
        }

        //health bar update

        healthBar.set(Constants.GAME_WIDTH - 450, 25, Constants.GAME_WIDTH - 25, 50);
        healthBar.set(Constants.GAME_WIDTH - 25 - (int)(425.0f*player.getHealth()/100), 25, Constants.GAME_WIDTH - 25, 50);

        if (printingInfo && System.currentTimeMillis() > printingInfoStart + printingInfoDuration) {
            printingInfo = false;
        }
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(actionPanelBitmap, 0, Constants.GAME_HEIGHT, null);
        canvas.drawBitmap(activeFireBitmap, 75, Constants.GAME_HEIGHT + 25, null);
        canvas.drawBitmap(activeShieldBitmap, Constants.PANEL_WIDTH - 75 - shieldButtonBitmap.getWidth(), Constants.GAME_HEIGHT + 25, null);

        //canvas.drawRect(healthBar, paint);
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawRect(Constants.GAME_WIDTH - 450, 25, Constants.GAME_WIDTH - 25, 45, paint);
        paint.setColor(Color.argb(150,0, 204, 0));
        paint.setStyle(Paint.Style.FILL);
        canvas.drawRect(Constants.GAME_WIDTH - 25 - (int)(425.0f*player.getHealth()/player.getMaxHealth()), 25, Constants.GAME_WIDTH - 25, 45, paint);

        for (int i = 0; i < player.getShields(); i++) {
            canvas.drawBitmap(shieldBitmap, Constants.GAME_WIDTH - 20 - (i + 1) * 60, 60, paint);
        }

        paint.setTextSize(36);
        paint.setColor(Color.WHITE);
        canvas.drawText("Ammo: " + Integer.toString(player.getAmmo()), 15, 100, paint);

        backButton.draw(canvas);

        paint.setTextAlign(Paint.Align.LEFT);
        paint.setTextSize(64);
        paint.setColor(Color.WHITE);
        if (mode == Mode.GAME_OVER) {
            text = "GAME OVER";
            paint.getTextBounds(text, 0, text.length(), textBounds);
            canvas.drawText(text, Constants.GAME_WIDTH / 2 - textBounds.width() / 2, (Constants.GAME_HEIGHT + Constants.PANEL_HEIGHT) / 2, paint);

            restartButton.setXY(Constants.GAME_WIDTH / 2 - restartButton.getWidth() / 2, (Constants.GAME_HEIGHT + Constants.PANEL_HEIGHT) / 2 + 60);
            restartButton.draw(canvas);
        }
        else if (mode == Mode.WIN){
            text = "YOU WIN!";
            paint.getTextBounds(text, 0, text.length(), textBounds);
            canvas.drawText(text, Constants.GAME_WIDTH / 2 - textBounds.width() / 2, (Constants.GAME_HEIGHT + Constants.PANEL_HEIGHT) / 2, paint);

            text = "Score " + Integer.toString(score);
            paint.setTextSize(48);
            paint.getTextBounds(text, 0, text.length(), textBounds);
            canvas.drawText(text, Constants.GAME_WIDTH / 2 - textBounds.width() / 2, (Constants.GAME_HEIGHT + Constants.PANEL_HEIGHT) / 2 + 60, paint);
            restartButton.setY((Constants.GAME_HEIGHT + Constants.PANEL_HEIGHT) / 2 + 120);
            nextLevelButton.setY((Constants.GAME_HEIGHT + Constants.PANEL_HEIGHT) / 2 + 120);
            restartButton.draw(canvas);
            nextLevelButton.draw(canvas);
        }
        else if (mode == Mode.BEFORE_START){
            text = "LEVEL " + levelNumber;
            paint.getTextBounds(text, 0, text.length(), textBounds);
            canvas.drawText(text, Constants.GAME_WIDTH / 2 - textBounds.width() / 2, (Constants.GAME_HEIGHT + Constants.PANEL_HEIGHT) / 2, paint);

            paint.setTextAlign(Paint.Align.LEFT);
            paint.setTextSize(48);
            paint.setColor(Color.WHITE);
            text = "Tap to start!";
            paint.getTextBounds(text, 0, text.length(), textBounds);
            canvas.drawText(text, Constants.GAME_WIDTH / 2 - textBounds.width() / 2, (Constants.GAME_HEIGHT + Constants.PANEL_HEIGHT )/ 2 + 60, paint);
        }

        if (printingInfo) {
            paint.setTextSize(48);
            paint.getTextBounds(info, 0, info.length(), textBounds);
            canvas.drawText(info, Constants.GAME_WIDTH / 2 - textBounds.width() / 2, (Constants.GAME_HEIGHT + Constants.PANEL_HEIGHT) / 5, paint);
        }

    }

    public void printInfo(String info, long timeMillis) {
        printingInfo = true;
        this.info = info;
        printingInfoStart = System.currentTimeMillis();
        printingInfoDuration = timeMillis;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public void setScore(int score) {
        this.score = score;
    }

}
