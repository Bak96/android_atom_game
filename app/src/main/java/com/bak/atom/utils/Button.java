package com.bak.atom.utils;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by Home on 2017-08-09.
 */

public class Button {
    private Rect bounds;
    private String text;
    private Paint borderPaint;
    private Paint borderSelected;
    private Paint backgroundPaint;

    private Paint textPaint;

    private Rect tempRect;


    private boolean selected;
    private boolean transparent;

    public Button(int x, int y) {
        bounds = new Rect(x, y, x, y);
        borderPaint = new Paint();
        borderPaint.setColor(Color.rgb(51, 128, 153));
        borderPaint.setStyle(Paint.Style.STROKE);
        borderSelected = new Paint();
        borderSelected.setColor(Color.WHITE);
        borderSelected.setStyle(Paint.Style.STROKE);

        textPaint = new Paint();
        textPaint.setColor(Color.rgb(51, 128, 153));
        textPaint.setTextSize(40);
        textPaint.setTextAlign(Paint.Align.LEFT);
        tempRect = new Rect();

        backgroundPaint = new Paint();
        backgroundPaint.setColor(Color.BLACK);
        backgroundPaint.setStyle(Paint.Style.FILL);

        selected = false;
        transparent = true;
    }

    public void draw(Canvas canvas) {
        if (!transparent) {
            canvas.drawRect(bounds, backgroundPaint);
        }


        if (selected) {
            canvas.drawRect(bounds, borderSelected);
        }
        else {
            canvas.drawRect(bounds, borderPaint);
        }

        textPaint.getTextBounds(text, 0, text.length(), tempRect);
        canvas.drawText(text, bounds.centerX() - tempRect.width() / 2, bounds.centerY() + tempRect.height() / 2, textPaint);
    }

    public void setHeight(int height) {
        bounds.set(bounds.left, bounds.top, bounds.right, bounds.top + height);
    }

    public void setWidth(int width) {
        bounds.set(bounds.left, bounds.top, bounds.right + width, bounds.bottom);
    }

    public void setXY(int x, int y) {
        bounds.set(x, y, x + bounds.right - bounds.left, y + bounds.bottom - bounds.top);
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean contains(int x, int y) {
        return bounds.contains(x, y);
    }

    public void setX(int x) {
        bounds.set(x, bounds.top, x + bounds.right - bounds.left, bounds.bottom);
    }

    public void setY(int y) {
        bounds.set(bounds.left, y, bounds.right, y + bounds.bottom - bounds.top);
    }

    public void down() {
        selected = true;
    }

    public void up() {
        selected = false;
    }

    public void setTransparent(boolean b) {
        transparent = b;
    }

    public int getWidth() {
        return bounds.width();
    }

    public int getHeight() {
        return bounds.height();
    }

}
