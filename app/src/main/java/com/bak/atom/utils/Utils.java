package com.bak.atom.utils;

import android.graphics.Canvas;
import android.graphics.Rect;

/**
 * Created by Home on 2017-08-05.
 */

public class Utils {

    public static String nextLevel(String level) {
        String next = "";
        int value = Integer.valueOf(level) + 1;

        if (value < 10) {
            next = "0";
        }

        next += Integer.toString(value);
        return next;
    }

    public static void setRectX(int x, Rect rect) {
        int width = rect.width();
        int height = rect.height();
        int top = rect.top;

        rect.set(x, top, x + width, top + height);
    }

    public static void setRectY(int y, Rect rect) {
        int width = rect.width();
        int height = rect.height();
        int left = rect.left;

        rect.set(left, y, left + width, y + height);
    }

    public static void setRectWidth(int width, Rect rect) {
        rect.set(rect.left, rect.top, rect.left + width, rect.bottom);
    }

    public static void setRectHeight(int height, Rect rect) {
        rect.set(rect.left, rect.top, rect.right, rect.top + height);
    }

}
