package com.bak.atom;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import com.bak.atom.utils.Vector2d;

/**
 * Created by Home on 2017-07-30.
 */

public class Box extends GameObject {
    private float width;
    private float height;

    public Box(int x, int y, float width, float height) {
        super(x, y);
        this.width = width;
        this.height = height;
        velocity = new Vector2d(0, 0);
        setColor(Color.GRAY);
    }

    @Override
    public void update() {
        position = position.add(velocity);
    }

    @Override
    public void draw(Canvas canvas) {
        Rect rect = new Rect();
        Paint p = new Paint();
        p.setColor(getColor());
        p.setStyle(Paint.Style.FILL);

        int left, top, right, bottom;
        left = (int) getPosition().getX();
        top = (int) getPosition().getY();
        right = (int)(getPosition().getX() + width);
        bottom = (int)(getPosition().getY() + height);

        rect.set(left, top, right, bottom);
        canvas.drawRect(rect, p);
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public void setHeight(float height) {
        this.height = height;
    }
}
