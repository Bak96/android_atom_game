package com.bak.atom;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

public class Chest extends Box {
    int health;
    int maxHealth;

    public Chest(int x, int y, float width, float height, int maxHealth) {
        super(x, y, width, height);
        health = maxHealth;
        this.maxHealth = maxHealth;
        setColor(Color.rgb(153, 102, 51));
    }

    public void hit(int value) {
        health -= value;

        if (health < 0) {
            health = 0;
        }
    }

    public int getHealth() {
        return health;
    }
    public int getMaxHealth() {
        return maxHealth;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);

        Rect rect = new Rect();
        Paint p = new Paint();
        p.setColor(Color.DKGRAY);
        p.setStyle(Paint.Style.FILL);

        int damageWidth = (int) (((maxHealth - health) / (float)maxHealth) * getWidth());
        int damageHeight = (int) (((maxHealth - health) / (float) maxHealth) * getHeight());
        int left = (int)(getPosition().getX() + (getWidth() - damageWidth) / 2);
        int top = (int)(getPosition().getY() + (getHeight() - damageHeight) / 2);
        int right = (int)(getPosition().getX() + getWidth() - (getWidth() - damageWidth) / 2);
        int bottom = (int)(getPosition().getY() + getHeight() - (getHeight() - damageHeight) / 2);

        rect.set(left, top, right, bottom);
        canvas.drawRect(rect, p);
    }
}
