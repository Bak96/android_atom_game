package com.bak.atom;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.bak.atom.managers.GameManager;

public class GamePanel extends SurfaceView implements SurfaceHolder.Callback {
    private MainThread thread;
    private GameManager gameManager;
    private AssetManager assetManager;
    private SharedPreferences sharedPreferences;

    public GamePanel(Context context, AssetManager assetManager, SharedPreferences sharedPreferences) {
        super(context);

        getHolder().addCallback(this);
        gameManager = new GameManager(this);
        this.assetManager = assetManager;
        this.sharedPreferences = sharedPreferences;
        thread = new MainThread(getHolder(), this);
        setFocusable(true);

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        gameManager.pushState(GameManager.State.SPLASH);
        thread = new MainThread(getHolder(), this);
        thread.setRunning(true);
        thread.start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;

        while (retry) {
            try {
                thread.setRunning(false);
                thread.join();
                retry = false;
            } catch(Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (gameManager.touched(event)) {
            return true;
        }

        return super.onTouchEvent(event);
    }

    public void update() {
        gameManager.update();
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);

        if(canvas!=null) {
            final int savedState = canvas.save();
            canvas.scale(Constants.SCALE_X, Constants.SCALE_Y);
            gameManager.draw(canvas);
            canvas.restoreToCount(savedState);
        }
    }

    public AssetManager getAssets() {
        return assetManager;
    }

    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

}
