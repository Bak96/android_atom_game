package com.bak.atom;

import android.graphics.Color;

import com.bak.atom.states.PlayState;
import com.bak.atom.utils.Vector2d;

public class Player extends Ball{
    private int health;
    private int maxHealth;
    private int shields;
    private int bullets;

    private long shieldStartTime; //millis
    private final long shieldDuration = 1500;
    private boolean usingShield = false;

    private long fireStartTime;
    private final long fireDuration = 500;
    private boolean firing;

    public final int MAX_SHIELDS = 7;
    public final int MAX_BULLETS = 50;

    //reference
    private PlayState playState;
    public Player(int x, int y, int radius, int maxHealth, int shields, int ammo, PlayState playState) {
        super(x, y, radius);
        setColor(Color.rgb(153, 102, 255));
        health = maxHealth;
        mass = 0.8f;
        this.maxHealth = maxHealth;
        this.playState = playState;

        this.shields = shields;
        this.bullets = ammo;
    }

    public int getHealth() {
        return health;
    }
    public int getMaxHealth() {
        return maxHealth;
    }

    public void setHealth(int x) {
        health = x;
    }

    public int getShields() {
        return shields;
    }

    public void setShields(int shields) {
        this.shields = shields;
    }

    public void useShield() {
        if (System.currentTimeMillis() - shieldStartTime < shieldDuration) {
            return;
        }

        if (shields == 0) {
            return;
        }

        shields--;
        shieldStartTime = System.currentTimeMillis();
        usingShield = true;
        setColor(Color.BLUE);
    }

    public void fire() {
        if (System.currentTimeMillis() - fireStartTime < fireDuration) {
            return;
        }

        if (bullets == 0) {
            return;
        }

        bullets -= 1;
        fireStartTime = System.currentTimeMillis();

        Vector2d bulletVelocity = new Vector2d(velocity);
        bulletVelocity = bulletVelocity.normalize().multiply(10).add(velocity);

        playState.spawnBullet((int)position.getX(), (int)position.getY(), 10, bulletVelocity);
        firing = true;
    }

    public void hit(int power) {
        health -= power;
        if (health < 0) {
            health = 0;
        }
    }

    public boolean isUsingShield() {
        return usingShield;
    }

    public void addShield(int num) {
        shields += num;
        if (shields > MAX_SHIELDS) {
            shields = MAX_SHIELDS;
        }
    }
    public void addAmmo(int num) {
        bullets += num;
        if (bullets > MAX_BULLETS) {
            bullets = MAX_BULLETS;
        }
    }

    public void update() {
        super.update();

        if (usingShield && System.currentTimeMillis() - shieldStartTime > shieldDuration) {
            usingShield = false;
            setColor(Color.rgb(153, 102, 255));
        }

        if (firing && System.currentTimeMillis() - fireStartTime > fireDuration) {
            firing = false;
        }
    }

    public boolean isFiring() {
        return firing;
    }

    public int getAmmo() {
        return bullets;
    }
}
