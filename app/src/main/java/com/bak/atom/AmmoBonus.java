package com.bak.atom;

import android.graphics.Color;

/**
 * Created by Home on 2017-08-09.
 */

public class AmmoBonus extends Bonus {
    public AmmoBonus(int x, int y, int radius) {
        super(x, y, radius);
        setColor(Color.YELLOW);
    }

    @Override
    public void applyBonus(Player player) {
        player.addAmmo(5);
    }
}
