package com.bak.atom.managers;

import com.bak.atom.Ball;
import com.bak.atom.Box;
import com.bak.atom.Constants;
import com.bak.atom.Player;
import com.bak.atom.utils.Vector2d;

public class CollisionManager {
    public CollisionManager() {

    }

    public boolean checkBorderHit(Ball b) {
        Vector2d position = b.getPosition();
        Vector2d velocity = b.getVelocity();
        float radius = b.getRadius();
        boolean hit = false;


        if (position.getX() + radius > Constants.GAME_WIDTH) {
            position.setX(Constants.GAME_WIDTH - radius);
            velocity.setX(-velocity.getX());
            hit = true;
        }

        if (position.getX() - radius < 0) {
            position.setX(radius);
            velocity.setX(-velocity.getX());
            hit = true;
        }

        if (position.getY() + radius > Constants.GAME_HEIGHT) {
            position.setY(Constants.GAME_HEIGHT - radius);
            velocity.setY(-velocity.getY());
            hit = true;
        }

        if (position.getY() - radius < 0) {
            position.setY(radius);
            velocity.setY(-velocity.getY());
            hit = true;
        }

        return hit;
    }

    public boolean colliding(Ball b1, Ball b2) {
        float xd = b1.getPosition().getX() - b2.getPosition().getX();
        float yd = b1.getPosition().getY() - b2.getPosition().getY();

        float sumRadius = b1.getRadius() + b2.getRadius();
        float sqrRadius = sumRadius * sumRadius;

        float distSqr = (xd * xd) + (yd * yd);

        if (distSqr <= sqrRadius)
        {
            return true;
        }

        return false;
    }

    public void resolveCollision(Ball b1, Ball b2) {
        // get the mtd
        Vector2d delta = (b1.getPosition().subtract(b2.getPosition()));
        float d = delta.getLength();
        // minimum translation distance to push balls apart after intersecting
        Vector2d mtd = delta.multiply(((b1.getRadius() + b2.getRadius())-d)/d);


        // resolve intersection --
        // inverse mass quantities
        float im1 = 1 / b1.getMass();
        float im2 = 1 / b2.getMass();

        // push-pull them apart based off their mass
        b1.setPosition(b1.getPosition().add(mtd.multiply(im1 / (im1 + im2))));
        b2.setPosition(b2.getPosition().subtract(mtd.multiply(im2 / (im1 + im2))));

        // impact speed
        Vector2d v = (b1.getVelocity().subtract(b2.getVelocity()));
        float vn = (float) v.dotProduct(mtd.normalize());

        // sphere intersecting but moving away from each other already
        if (vn > 0.0f) return;

        // collision impulse
        float i = (-(1.0f + Constants.restitution) * vn) / (im1 + im2);
        Vector2d impulse = mtd.multiply(i);

        // change in momentum
        b1.setVelocity(b1.getVelocity().add(impulse.multiply(im1)));
        b2.setVelocity(b2.getVelocity().subtract(impulse.multiply(im2)));
    }

    public void resolveCollision2(Player player, Ball b) {
        // get the mtd
        Vector2d delta = (player.getPosition().subtract(b.getPosition()));
        float d = delta.getLength();
        // minimum translation distance to push balls apart after intersecting
        Vector2d mtd = delta.multiply(((player.getRadius() + b.getRadius())-d)/d);

        // resolve intersection --
        // inverse mass quantities
        float im1 = 1 / player.getMass();
        float im2 = 1 / b.getMass();

        // push-pull them apart based off their mass
        player.setPosition(player.getPosition().add(mtd.multiply(im1 / (im1 + im2))));
        b.setPosition(b.getPosition().subtract(mtd.multiply(im2 / (im1 + im2))));

        b.setVelocity(delta.normalize().multiply(-1 * b.getVelocity().getLength()));
    }

    public boolean colliding(Ball ball, Box box) {
        float distanceX = Math.abs(ball.getPosition().getX() - (box.getPosition().getX() + box.getWidth() / 2));
        float distanceY = Math.abs(ball.getPosition().getY() - (box.getPosition().getY() + box.getHeight() / 2));

        if (distanceX > (box.getWidth()/2 + ball.getRadius())) { return false; }
        if (distanceY > (box.getHeight()/2 + ball.getRadius())) { return false; }

        if (distanceX <= (box.getWidth()/2)) { return true; }
        if (distanceY <= (box.getHeight()/2)) { return true; }

        float cornerDistance_sq = (distanceX - box.getWidth()/2)*(distanceX - box.getWidth()/2) +
                    (distanceY - box.getHeight()/2)*(distanceY - box.getHeight()/2);

        return (cornerDistance_sq <= (ball.getRadius() * ball.getRadius()));
    }

    public void resolveCollision(Ball ball, Box box) {
        Vector2d ballVelocity = ball.getVelocity();
        Vector2d ballPosition = ball.getPosition();
        Vector2d mtd;
        Vector2d newPosition;

        //upperLeftCorner
        if (ballPosition.getX() < box.getPosition().getX()
                && ballPosition.getY() < box.getPosition().getY()) {
            Vector2d corner = new Vector2d(box.getPosition().getX(), box.getPosition().getY());
            mtd = ballPosition.subtract(corner);
            mtd = mtd.normalize();
            mtd = mtd.multiply(ball.getRadius() + 1);

            newPosition = corner.add(mtd);
            ball.setPosition(newPosition);
            ball.setVelocity(mtd.normalize().multiply(ballVelocity.getLength()));
            //System.out.println("hit upperLeftCorner");
        }
        //upperRightCorner
        else if (ballPosition.getX() > box.getPosition().getX() + box.getWidth()
            && ballPosition.getY() < box.getPosition().getY()) {
            Vector2d corner = new Vector2d(box.getPosition().getX() + box.getWidth(), box.getPosition().getY());
            mtd = ballPosition.subtract(corner);
            mtd = mtd.normalize();
            mtd = mtd.multiply(ball.getRadius() + 1);

            newPosition = corner.add(mtd);
            ball.setPosition(newPosition);
            ball.setVelocity(mtd.normalize().multiply(ballVelocity.getLength()));
            //System.out.println("hit upperRightCorner");
        }
        //bottomLeftCorner
        else if (ballPosition.getX() < box.getPosition().getX()
            && ballPosition.getY() > box.getPosition().getY() + box.getHeight()) {
            Vector2d corner = new Vector2d(box.getPosition().getX(), box.getPosition().getY() + box.getHeight());
            mtd = ballPosition.subtract(corner);
            mtd = mtd.normalize();
            mtd = mtd.multiply(ball.getRadius() + 1);

            newPosition = corner.add(mtd);
            ball.setPosition(newPosition);
            ball.setVelocity(mtd.normalize().multiply(ballVelocity.getLength()));
            //System.out.println("hit bottomLeftCorner");
        }
        //bottomRightCorner
        else if (ballPosition.getX() > box.getPosition().getX() + box.getWidth()
            && ballPosition.getY() > box.getPosition().getY() + box.getHeight()) {
            Vector2d corner = new Vector2d(box.getPosition().getX() + box.getWidth(), box.getPosition().getY() + box.getHeight());
            mtd = ballPosition.subtract(corner);
            mtd = mtd.normalize();
            mtd = mtd.multiply(ball.getRadius() + 1);

            newPosition = corner.add(mtd);
            ball.setPosition(newPosition);
            ball.setVelocity(mtd.normalize().multiply(ballVelocity.getLength()));
           // System.out.println("hit bottomRightCorner");
        }
        //up
        else if (ballPosition.getY() < box.getPosition().getY()) {
            ballPosition.setY(box.getPosition().getY() - ball.getRadius() - 1);
            ballVelocity.setY(-1 * ballVelocity.getY());
          //  System.out.println("hit up");
        }
        //left
        else if (ballPosition.getX() < box.getPosition().getX()) {
            ballPosition.setX(box.getPosition().getX() - ball.getRadius() - 1);
            ballVelocity.setX(-1 * ballVelocity.getX());
            //System.out.println("hit left");
        }
        //right
        else if (ballPosition.getX() > box.getPosition().getX() + box.getWidth()) {
            ballPosition.setX(box.getPosition().getX() + box.getWidth() + ball.getRadius() + 1);
            ballVelocity.setX(-1 * ballVelocity.getX());
            //System.out.println("hit right");
        }
        //down
        else {
            ballPosition.setY(box.getPosition().getY() + box.getHeight() + ball.getRadius() + 1);
            ballVelocity.setY(-1 * ballVelocity.getY());
           // System.out.println("hit down");
        }
    }

    public void resolveCollision2(Ball ball, Box box) {
        Vector2d ballVelocity = ball.getVelocity();
        Vector2d ballPosition = ball.getPosition();

        //A---B
        //|---|
        //D---C
        Vector2d A = new Vector2d(box.getPosition());
        Vector2d B = new Vector2d(box.getPosition().getX() + box.getWidth(), box.getPosition().getY());
        Vector2d C = new Vector2d(box.getPosition().getX() + box.getWidth(), box.getPosition().getY() + box.getHeight());
        Vector2d D = new Vector2d(box.getPosition().getX(), box.getPosition().getY() + box.getHeight());
        boolean isAboveAC = isOnUpperSideOfLine(C, A, ball.getPosition());
        boolean isAboveDB = isOnUpperSideOfLine(B, D, ball.getPosition());

        //uderzylo z gory
        if (isAboveAC && isAboveDB) {
            ballPosition.setY(box.getPosition().getY() - ball.getRadius() - 1);
            ballVelocity.setY(-1 * ballVelocity.getY());
        }
        //uderzylo z prawej
        else if (isAboveAC && !isAboveDB) {
            ballPosition.setX(box.getPosition().getX() + box.getWidth() + ball.getRadius() + 1);
            ballVelocity.setX(-1 * ballVelocity.getX());
        }
        //uderzylo z lewej
        else if (!isAboveAC && isAboveDB) {
            ballPosition.setX(box.getPosition().getX() - ball.getRadius() - 1);
            ballVelocity.setX(-1 * ballVelocity.getX());
        }
        //uderzylo z dolu
        else if (!isAboveAC && !isAboveDB) {
            ballPosition.setY(box.getPosition().getY() + box.getHeight() + ball.getRadius() + 1);
            ballVelocity.setY(-1 * ballVelocity.getY());
        }
    }

    private boolean isOnUpperSideOfLine(Vector2d corner, Vector2d oppositeCorner, Vector2d ballCenter)
    {
        return ((oppositeCorner.getX() - corner.getX()) * (ballCenter.getY() - corner.getY()) - (oppositeCorner.getY() - corner.getY()) * (ballCenter.getX() - corner.getX())) > 0;
    }
}
