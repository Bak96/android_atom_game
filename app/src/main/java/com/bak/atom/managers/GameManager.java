package com.bak.atom.managers;

import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.view.MotionEvent;

import com.bak.atom.GamePanel;
import com.bak.atom.MainActivity;
import com.bak.atom.states.EndlessState;
import com.bak.atom.states.GameState;
import com.bak.atom.states.LeaderboardsState;
import com.bak.atom.states.LevelSelectState;
import com.bak.atom.states.MenuState;
import com.bak.atom.states.LevelState;
import com.bak.atom.states.SplashState;

import java.util.Stack;

/**
 * Created by Home on 2017-08-04.
 */

public class GameManager {
    Stack<GameState> states;

    //refererence
    GamePanel gamePanel;

    public enum State{
        SPLASH,
        MENU,
        LEVEL_SELECT,
        LEADERBOARDS,
        ENDLESS,
        PLAY
    }


    public GameManager(GamePanel gamePanel) {
        states = new Stack();
        this.gamePanel = gamePanel;
    }

    public void pushState(State state) {
        switch (state) {
            case MENU:
                states.push(new MenuState(this));
                break;
            case SPLASH:
                states.push(new SplashState(this));
                break;
            case LEVEL_SELECT:
                states.push(new LevelSelectState(this));
                break;
            case LEADERBOARDS:
                states.push(new LeaderboardsState(this));
                break;
            case ENDLESS:
                states.push(new EndlessState(this));
                break;
        }
    }

    public void playState(String level) {
        states.push(new LevelState(this, level));
    }

    public void clearStates() {
        states.clear();
    }

    public boolean touched(MotionEvent event) {
        return states.peek().touched(event);
    }

    public Resources getResources() {
        return gamePanel.getResources();
    }

    public AssetManager getAssetManager() {
        return gamePanel.getAssets();
    }

    public SharedPreferences getSharedPreferences() {
        return gamePanel.getSharedPreferences();
    }

    public void update() {
        try {
            states.peek().update();
        } catch (Exception ex) {ex.printStackTrace();}

    }

    public void draw(Canvas canvas) {
        try {
            states.peek().draw(canvas);
        } catch (Exception ex) {ex.printStackTrace();}

    }

    public void exit() {
        MainActivity host = (MainActivity) gamePanel.getContext();
        host.finish();
    }

}
