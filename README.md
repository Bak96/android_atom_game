#Atom game

Gra polega na zebraniu klucza, który jest w jednej ze skrzynek (brązowy prostokąt = skrzynka). Skrzynki niszczy się pociskami, które można wystrzelić żółtym przyciskiem. Gracz (fioletowa kulka) porusza się w kierunku wskazanym przez tapnięcie w dowolne miejsce ekranu. Gracz traci pkty życia po zderzeniu z przeciwnikiem (czerwona kulka). Jeśli pasek życia spadnie do 0 to gracz przegrywa. Gracz nie traci pktów życia przy zderzeniu jeśli użyje tarczy (niebieski przycisk). Nie można bezpośrednio sterować prędkością kulki. Przy zderzeniach obliczane są nowe prędkości zgodnie z zasadami zachowania energii i pędu. Gracz ma ograniczoną ilość dostępnych tarcz i amunicji. Zderzenie gracza z żółtą kulką dodaje amunicję, a z niebieską dodaje tarczę. Są to bonusy, które pojawiają się losowo.

Obecnie w grze są 2 tryby - zwykły i endless. W trybie endless punkty dostaje się za ilość zebranych kluczy. Zwykły tryb na ten moment to przygotowany zestaw 25 leveli, które wygrywa się zdobywając klucz. Punkty w zwykłym trybie są wyliczane na podstawie celności i pozostałego życia gracza.


![screenshot](https://bytebucket.org/Bak96/android_atom_game/raw/bd3713fcec4e2d302b2c895d906a81e0c3d2af3d/screenshots/main.png)

![screenshot](https://bytebucket.org/Bak96/android_atom_game/raw/821bc36e2d9b41cc363e18441e1b2640965df3df/screenshots/select.png)

![screenshot](https://bytebucket.org/Bak96/android_atom_game/raw/821bc36e2d9b41cc363e18441e1b2640965df3df/screenshots/example.png)

![screenshot](https://bytebucket.org/Bak96/android_atom_game/raw/821bc36e2d9b41cc363e18441e1b2640965df3df/screenshots/leaderboards.png)


